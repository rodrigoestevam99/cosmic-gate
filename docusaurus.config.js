const math = require('remark-math');
const katex = require('rehype-katex');

module.exports = {
  title: 'Raios Cósmicos nas Escolas',
  tagline: 'Uma rede de colaboração científica entre escola e universidade',
  url: 'https://rodrigo99.gitlab.io/cosmic-gate/',
  baseUrl: '/cosmic-gate/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/logo.svg',
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.
  stylesheets: [
    {
      href: 'https://cdn.jsdelivr.net/npm/katex@0.12.0/dist/katex.min.css',
      type: 'text/css',
      integrity:
        'sha384-AfEj0r4/OFrOo5t7NnNe46zW/tFgW6x/bCJG8FqQCEo3+Aro6EYUG4+cU+KJWu/X',
      crossorigin: 'anonymous',
    },
  ],
  themeConfig: {
    navbar: {
      title: 'Raios Cósmicos nas Escolas',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.svg',
      },
      items: [
        {to: 'docs/',
          activeBasePath: 'docs',
          label: 'Para saber mais',
          position: 'left',
        },
        {to: 'docs/atividades/atividades', label: 'Atividades', position: 'left'},
        {to: 'docs/tecnica', label: 'Documentação técnica', position: 'left'},
        {to: 'docs/pedagogico', label: 'Projeto pedagógico', position: 'left'},
        {to: 'docs/oficinas', label: 'Oficinas', position: 'left'},
        {to: 'docs/professores', label: 'Professores e escolas', position: 'left'},
        {
          href: 'https://github.com/facebook/docusaurus',
          label: 'GitHub',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Introdução',
          items: [
            {
              label: 'Partículas cósmicas',
              to: 'docs/fundamentos/origem/',
            },
            {
              label: 'Pesquisa',
              to: 'docs/fundamentos/pioneiros/',
            },
            {
              label: 'Nosso experimento',
              to: 'docs/fundamentos/hardware/',
            },
            {
              label: 'Tutoriais',
              to: 'docs/atividades/atividades',
            },
            {
              label: 'Documentação técnica',
              to: 'docs/tecnica/',
            },
          ],
        },
        {
          title: 'Comunidade',
          items: [
            {
              label: 'Escolas',
              href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            },
            {
              label: 'Universidades',
              href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            },
            {
              label: 'IPPOG',
              href: 'https://ippog.org/',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Twitter',
              href: 'https://twitter.com/',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Raios Cosmicos nas Escolas, Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
          remarkPlugins: [math],
          rehypePlugins: [katex],
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
