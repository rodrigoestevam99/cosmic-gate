import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import {Feature} from '../components/feature';
import styles from './styles.module.css';
import classnames from 'classnames';
const features = [
  {/*
    title: 'Easy to Use',
    imageUrl: 'img/undraw_docusaurus_mountain.svg',
    description: (
      <>
        Docusaurus was designed from the ground up to be easily installed and
        used to get your website up and running quickly.
      </>
    ),*/
  },
  {
    
    /*
    imageUrl: 'img/undraw_Outer_space_drqu.svg'
    title: 'Focus on What Matters',
    imageUrl: 'img/undraw_docusaurus_tree.svg',
    description: (
      <>
        Docusaurus lets you focus on your docs, and we&apos;ll do the chores. Go
        ahead and move your docs into the <code>docs</code> directory.
      </>
    ),*/
  },
  {/*
    title: 'Powered by React',
    imageUrl: 'img/undraw_docusaurus_react.svg',
    description: (
      <>
        Extend or customize your website layout by reusing React. Docusaurus can
        be extended while reusing the same header and footer.
      </>
    ),*/
  },
];

function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;

  return (
    <Layout
      description={siteConfig.tagline.replace("<br/>", " ")}
    >
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle" dangerouslySetInnerHTML={{__html: siteConfig.tagline}}></p>
        </div>
      </header>
      <main>
        {features && features.length && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map(({ imageUrl, title, description }, idx) => (
                  <div
                    key={idx}
                    className={classnames('col col--4', styles.feature)}
                  >
                    {imageUrl && (
                      <div className="text--center">
                        <img
                          className={styles.featureImage}
                          src={useBaseUrl(imageUrl)}
                          alt={title}
                        />
                      </div>
                    )}
                    <h3>{title}</h3>
                    <p>{description}</p>
                  </div>
                ))}
              </div>
            </div>
          </section>
        )}

      <div className="container">
      <Feature
        img={<img src={useBaseUrl('img/outer_rays.png')} alt="Annotations" loading="lazy" />}
        title="Por que detectar raios cósmicos?"
        text={
          <>
            <p>
            Raios Cósmicos estão em todo lugar!
            </p>
            <p>
            Em cada metro quadrado, a cada segundo, centenas destas partículas cósmicas de altas energias nos alcançam.
            O que as medidas destas partículas têm a nos ensinar sobre processos que ocorrem em nossa galáxia ou até 
            mesmo em galáxias distantes? Como os raios cósmicos afetam a nossa vida na Terra?
            </p>
          </>
        }
      />
      <Feature
        img={<img src={useBaseUrl('img/undraw_connected_world_wuay.svg')} alt="Always Compare" loading="lazy" />}
        title="Nossa proposta"
        reversed
        text={
          <>
            <p>
            Criar uma rede de detectores de partículas que podem ser construídos e operados por alunos e professores do ensino médio. O projeto permite investigação autônoma e envolvimento dos alunos em atividade de pesquisa científica de tópicos da física de partículas, astronomia e computação, através de atividades "hands-on".
            </p>
          </>
        }
      />
      <Feature
        img={<img src={useBaseUrl('img/undraw_google_docs_jf93.svg')} alt="Aggregation and rich KPIs" loading="lazy" />}
        title="Quem participa?"
        text={
          <>
            <p>
            Alunos e professores de ensino médio, pesquisadores em educação e físicos experimentais de altas energias, unindo escolas, universidades e institutos de pesquisa.            
            </p>

          </>
        }
      />
    </div>


      {/* <div className="container">
        <div className="row">
          <div className="col col--6 col--offset-3 padding-vert--lg">
            <h2>Introduction Video</h2>
            <iframe
              width="100%"
              height="315"
              src="https://www.youtube.com/embed/nYkdrAPrdcw"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            />
            <div className="text--center padding-vert--lg">
              <Link
                className="button button--primary button--lg"
                to={useBaseUrl('docs/introduction')}
              >
                Learn more about QA-Board!
              </Link>
            </div>
          </div>
        </div>
      </div> */}
      </main>
    </Layout>
  );
}

export default Home;