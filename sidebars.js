module.exports = {
  someSidebar: {
    "Fundamentos": [
      "fundamentos/descoberta",
      {
        "type": "category",
        "label": "Partículas cósmicas",
        "items": ['fundamentos/origem']
      },
      {
        "type": "category",
        "label": "Pesquisa",
        "items": ['fundamentos/pioneiros',
                  'fundamentos/aparatos',
                  'fundamentos/colaboracoes']
      },
      {
        "type": "category",
        "label": "Nosso expermiento",
        "items": ['fundamentos/deteccao',
                  'fundamentos/hardware']
      }
    ],
    "Informação complementar": [
      {
        "type": "category",
        "label": "Anatomia das Partículas Cósmicas",
        "items": ['fundamentos/anatomia',
                  'fundamentos/sm',
                  'fundamentos/colaboracoes']
      }
    ],
  },
  secodnSidebar : {
    "Atividades": ["atividades/atividades"]
  }

};
