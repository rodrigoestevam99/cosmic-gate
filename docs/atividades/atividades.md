---
id: atividades
title: Atividades e ambiente de programação
---
import useBaseUrl from '@docusaurus/useBaseUrl';

## JupyterHub - Ambiente de programação

<p align="center">
<img alt="save-as-milestone" src={useBaseUrl('img/servidor.png')} width="200" />
</p>



**O Jupyterhub** é um ambiente de programação que pode ser hospedado em um servidor.
Este ambiente terá conectividade com a base de dados gerada pelos detectores, assim, por meio de atividades, os alunos poderão interagir com tais dados.

Como todo o processamento é feito no servidor, qualquer tipo de computador com acesso a internet, sendo "potente" ou não, consegue acessar as atividades.

Para acessar nosso servidor, [clique aqui](http://raioscosmicos.if.usp.br/jupyter)

## Lista de atividades

### Análise

* Atividade 1: Histogramas
  

* Atividade 2: Monitorando o experimento
  * Dashboard
  
* Atividade 3: Explorando os dados

* Atividade X: Simulando raios cósmicos

* Atividade 4: Determinando fluxo de raios cósmicos da sua estação

* Atividade 5: Integrando os dados de duas estações

* Atividade 6: Influência das condições climáticas no fluxo de raios cósmicos

* Atividade 7: Distribuição angular do fluxo de raios cósmicos

* Atividade 8: Selecionando os eventos de raios cósmicos

### Interação com o detector

* Atividade 1: Entendendo o detector

* Atividade 2: Como eu sei que o que estou detectando são raios cósmicos?

* Atividade 3: Calibração do detector

* Atividade X: Operando uma estação de raios cósmicos
