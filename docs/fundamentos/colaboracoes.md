---
id: colaboracoes
title: Experimentos da atualidade
sidebar_label: Experimentos da atualidade
---
import useBaseUrl from '@docusaurus/useBaseUrl';

O trabalho de Lattes em Berkeley demonstrou que a exploração das partículas elementares não se restringia apenas a medidas de  interações das partículas cósmicas.  A partir da década de 1950 os aceleradores de partículas entraram fortemente em operação, permitindo aos cientistas  o desenvolvimento de novos experimentos e sitemas de aquisição de dados  de alta estatística, com controle dos tipos de partículas primárias e energias definidas. 

Nas útlimas décadas, o enorme progresso na física de partículas elementares se deu através dos  instrumentos desenvolvidos e aperfeiçoados para os experimentos com partículas cósmicas e, posteriormente, também para aceleradores: eletrômetros, câmaras de nuvens, contadores Geiger-Müller, circuitos de coincidência, emulsões fotográficas,  fotomultiplicadoras, cintiladores, detectores de radiação Cherenkov,
calorímetros e  detectores de traços.   

Apesar de todo  avanço do conhecimento científico graças à pesquisa com raios cósmicos,  muitas questõs cosmológicas ainda necessitam de resposta.

* Qual é a fonte dos raios cósmicos? 
* O que está lançando partículas altamente energéticas pelo universo?  
- Será a fonte uma explosão cósmica extremamente potente e desconhecida? 
- Ou será um enorme buraco negro que suga estrelas até que morram violentamente?  
- De galáxias em colisão? 
* Será que os raios cósmicos podem dar uma resposta sobre a natureza da matéria escura?
 * Será que os raios cósmicos influenciam na formação de nuvens, nas condições climáticas?
 * Qual a fração de partículas  de antimatéria na radiação primária?


Para responder a estas questões e tantos outros mistérios, muitas colaborações mundiais de físicos de astropartículas foram formadas e  experimentos maiores e mais sensíveis estão operando ou se encontram em fase de construção. Entre os inúmeros esforços  de mais de 100 anos de descoberta dos raios cósmicos, podemos citar os mais recentes: IceCube e IceTop no Pólo Sul, VERITAS (_Very Energetic Radiation Imaging Telescope Array System_) nos EUA, MAGIC (_Major Atmospheric Gamma Imaging Cherenkov_) em La Palma,  H.E.S.S. (_High Energy Stereoscopic System_) na Namíbia, CTA (_Cherenkov Telescope Array_) , Experimento Auger na Argentina, bem como experimentos em órbita como o espectrômetro AMS,  conectado em um módulo do lado de fora da Estação Espacial Internacional (ISS), e que procura por matéria escura, antimatéria, além  realizar medições precisas de raios cósmicos.
