---
id: origem
title: Origens e características
sidebar_label: Origens e características
---
import useBaseUrl from '@docusaurus/useBaseUrl';


Descobertos em 1912 pelo físico austríaco Victor Hess, os raios cósmicos são partículas relativísticas (em sua maior parte prótons e núcleos leves)   produzidas por supernovas e outras fontes em nossa galáxia e além dela. 

Evidências experimentais sugerem que os raios cósmicos de energia até $3 \times 10^18$ eV são criados e acelerados em explosões estelares na Via Láctea. Já os mais energéticos, (milhões de vezes mais energéticos do que os prótons que são acelerados no LHC a $7\times10^12$ eV)  devem ter origem fora de nossa galáxia.  Como os raios cósmicos alcançam energias tão altas? Onde estão os aceleradores naturais? Sabemos por exemplo, que entre os raios cósmicos de baixa energia, existem aqueles que chegam do Sol em um fluxo de partículas carregadas conhecido como vento solar, e que são responsáveis pelo espetáculo de luzes das auroras boreais.  Entretanto, torna-se  mais difícil a determinação  da origem de partículas de alta energia na medida que são influenciadas pelos campos magnéticos do espaço interestelar.

<p align="center">
<img src={useBaseUrl('img/aurora.jpg')} width="200"/>
</p>

Para aprender sobre a natureza dos raios cósmicos de alta energia, os cientistas medem sua energia e sua direção conforme chegam do espaço. Os raios cósmicos de energia mais baixa são medidos diretamente pelo envio de detectores a alturas elevadas na atmosfera terrestre, pelo uso de  balões e satélites. Para raios cósmicos de alta energia, entretanto, é mais eficiente explorar a atmosfera, medindo cada raio cósmico indiretamente, observando a chuva de partículas que ele produz no ar.



Quando uma destas partículas  colide com uma molécula na atmosfera da Terra,  uma cascata de partículas secundárias é gerada. Por exemplo, um próton vindo do espaço atinge um próton (ou um nêutron) num núcleo atômico do ar atmosférico (oxigênio ou nitrogênio). Parte da energia do próton incidente (aproximadamente 40%)  se transfoma gerando novas partículas (secundárias) com suas respectivas massas e energias.

<p align="center">
<img src={useBaseUrl('img/cosmic_ray.jpg')} width="200"/>
</p>

A cascata de partículas secundárias, também denominada radiação secundária, é formada em sua maior parte por prótons, elétrons e múons. Esta radiação secundária pode também disparar mais transformações nucleares que por sua vez dão origem a mais núcleos radioativos. O isótopo de carbono C-14, importante para a datação de fósseis, é obtido a partir destas transformações. 

### Múons penetrantes da radiação cósmica

O múon é uma partícula elementar subatômica similar ao elétron, mas mais massiva (massa_{\mu} \appox 207 \times massa_{elétron}). Produzido na atmosfera superior pelo decaimento de píons produzidos por raios cósmicos primários:

$\pi^{+} \rightarrow \mu^{+} + \nu_{\mu}$ \\quad $\pi^{-} \rightarrow \mu^{-} + \bar{\nu}_{\mu}$

apresenta duas formas: uma com carga negativa e outra, sua anti-partícula, positivamente carregada. 

O muon foi descoberto como um constituinte dos "chuveiros" de partículas de raios cósmicos em 1936 pelos físicos americanos Carl D. Anderson e Seth Neddermeyer. Por causa de sua massa, a princípio pensou-se que fosse a partícula prevista pelo
físico japonês Yukawa Hideki em 1935 para explicar a interação forte que mantêm prótons e nêutrons unidos no núcleo.   Posteriormente descobriu-se que o múon pertence ao grupo de partículas subatômicas denominadas léptons, que não reagem com o núcleo ou com outras partículas através da interação forte. Os múons são relativamente instáveis, com um tempo de vida de $2.2 \times 10^{-6}$ s antes que decaiam, via interação fraca, em um elétron e dois tipos de neutrinos. Por causa de sua carga elétrica, antes de decairem, os múons perdem energia deslocando elétrons dos átomos (ionização). Contudo, como viajam em velocidades próximas a da luz, o processo de ionização dissipa a energia em quantidades relativamente pequenas, garantindo que os múons  possam ainda viajar  abaixo da superfície da Terra.  Assim, os múons penetrantes da radiação cósmica confirmam a dilatação do tempo prevista por Einstein na formulação da teoria da relatividade.










