---
id: hardware
title: Hardware
---
import useBaseUrl from '@docusaurus/useBaseUrl';

## Fotos

### Vista superior
<p align="center">
<img src={useBaseUrl('img/vista_superior.jpg')} width="500"/>
</p>


### Vista isométrica
<p align="center">
<img src={useBaseUrl('img/vista_isometrica.jpg')} width="500"/>
</p>

### Vista explodida
<p align="center">
<img src={useBaseUrl('img/vista_explodida.jpg')} width="500"/>
</p>