---
id: aparatos
title: Como detectamos raios cósmicos
sidebar_label: Como detectamos raios cósmicos
slug: /
---
import useBaseUrl from '@docusaurus/useBaseUrl';

Os detectores de raios cósmicos são detectores de partículas que utilizam um mesmo princípio fundamental: uma partícula não se torna evidente até que interaja com o material do aparato experimental de forma mensurável ou decaia em outras partículas detectáveis. 
Isto ocorre a partir da  transferência de uma parte ou de toda a energia da(s) partícula(s) que atravessa(m) o volume do detector,  convertida posteriormente em alguma outra forma mais acessível para "percepção" humana. A forma na qual a energia é convertida dependerá do tipo de detector e de seu projeto.  Através  de medidas das propriedades fundamentais das partículas como massa e carga elétrica e as leis de conservação poderemos, por exemplo, identificá-las e determinar  sua direção de chegada.     



## A detecção direta com uso de eletrômetros e contadores Geiger-Müller

 
Os vôos  em balões  tripulados  permitiram a detecção direta dos raios cósmicos com a utilização de eletrômetros.  Um eletrômetro padrão é composto de duas finas folhas condutoras de massa muito pequena suspensas em um eletrodo. Quando o eletrodo está carregado, as folhas têm cargas de mesmo sinal e se repelem. Quanto maior a carga, mais as folhas se distanciam. No eletroscópio de Wulf: as folhas do eletroscópio são substituídas por dois fios condutores, o que permite medir pequenas quantidades de carga; as medidas da radiação ionizante são feitas em um detector de ionização e no eletroscópio; um microscópio facilita as leituras.

<p align="center">
<img src={useBaseUrl('img/eletrometro.png')} width="200"/>
</p>

No início do século XX, o eletrômetro era o instrumento padrão utilizado para estudo da 
radioatividade e da condutividade do ar.  Era sabido que a radioatividade ioniza o
ar (ou gases em geral) e portanto, iria sensibilizar um eletrômetro se este se encontrasse nas proximidades de uma fonte radioativa. Consequentemente, o transporte de eletrômetros para altitudes mais elevadas no estudo de raios cósmcos foi a estratégia da pesquisa experimental na época. 

Um próximo avanço foi alcançado por  Hess em 1912 ao usar   eletrômetros  selados.   Com esta condição, a densidade do número de partículas dentro do aparelho foi mantida constante, apesar da variação da temperatura ambiente e pressão do ar durante a subida de balão. Hess teve especial cuidado com as condições experimentais: procurou minimizar a radiação de seu próprio dispositivo de medida, um eletrômetro de Wulf associado a um detector de ionização; ele considerou variáveis meteorológicas (pressão, temperatura, umidade etc.), o que exigiu que fosse levada a bordo instrumentação suplementar. Em seu artigo _Sobre a observação da radiação penetrante em sete campanhas de balão_ (V. Hess, Physikalische Zeitschrschift 13, 1084 (1912)). Hess realizou medições com três eletrômetros independentes durante o vôo.

Posteriormente os eletroscópios foram substituídos por um novo tipo de detector: os tubos Geiger-Müller.  Em 1929, Hans Geiger e Walter Müller desenvolveram um detector de ionização cheio de gás - um tubo que registra partículas individuais carregadas. Este contador Geiger-Müller era ideal para estudar raios cósmicos de alta energia.  O detector  assume comumente a forma de um invólucro externo cilíndrico (cátodo) e o espaço selado preenchido com gás, com um fio central fino (o ânodo) mantido a uma voltagem positiva de $\approx 1$ kV em relação ao cátodo. O gás do invólucro é geralmente argônio a uma pressão de menos de 0,1 atm mais uma pequena quantidade de um vapor.  Se um raio gama interagir com o tubo  Geiger-Müller (principalmente com a parede pelo efeito fotoelétrico ou espalhamento Compton), ele produzirá um elétron energético que pode atravessar o interior do tubo. A ionização ao longo do caminho do elétron primário resulta em elétrons de baixa energia que serão acelerados em direção ao fio central pelo forte campo elétrico. Colisões com o gás do interior do tubo  produzem estados excitados ($\approx$ 11,6eV) que decaem com a emissão de um fóton de ultra-violeta e pares elétron-íon (~ 26,4 eV para o argônio). Os novos elétrons, mais o original, são acelerados para produzir uma cascata de ionização chamada "multiplicação de gás" ou avalanche de _Townsend_. O fator de multiplicação para uma avalanche varia entre  $10^6$ a $10^8$. Os fótons emitidos podem ionizar diretamente as moléculas de gás ou atingir a parede do cátodo, liberando elétrons adicionais que rapidamente produzem avalanches adicionais. Assim, uma densa camada de ionização se propaga ao longo do fio central em ambas as direções, longe da região de excitação inicial, produzindo o que é denominado uma descarga Geiger-Mueller.

Um passo essencial em direção aos balões não tripulados com uma leitura automática dos dispositivos de medição foi a invenção da técnica de coincidência,  relatada por W. Bothe e W. Kolhörster em _The nature of the high-altitude radiation_ em 1929. Dois tubos Geiger-Müller foram operados em coincidência com um absorvedor de metal entre os dois tubos e a intensidade da radiação penetrante foi medida em função da espessura do material absorvedor. Um raio gama só dispara um contador Geiger se retirar um elétron de um átomo. A observação de sinais coincidentes sugere que um raio gama cósmico produziu dois elétrons ou que um único elétron disparou os dois contadores. Para testar se era um elétron que havia acionado os dois contadores, Bothe e Kolhörster posicionaram uma peça de ouro de 4 cm de espessura entre os contadores para absorver os elétrons expulsos dos átomos. Eles descobriram que os raios não foram afetados e concluíram que os raios cósmicos eram constituídos de partículas eletricamente carregadas e não eram raios gama. Pela descoberta da técnica da coincidência, W. Bothe recebeu o Nobel Prêmio em 1954.


A exploração da estratosfera com os vôos tripulados por Auguste Piccard  se tornaram lendários. Piccard tornou possível a aviação moderna e a exploração espacial ao inventar a cabine pressurizada e o balão estratosférico. Sempre testando suas próprias invenções, ele fez as duas primeiras subidas à estratosfera (atingindo altitudes de 15.780 metros em 1931 e 16.201 metros em 1932), durante as quais estudou os raios cósmicos e se tornou o primeiro homem a testemunhar a curvatura da Terra com seus próprios olhos. Pela primeira vez, um ser humano havia entrado na estratosfera e provado que era possível sobreviver por muito tempo acima do nível de 5.000 metros, considerado na época uma barreira impenetrável.

Créditos das imagens:
_Bertrand Piccard -changer d'altitude_ in https://bertrandpiccard.com/family-tradition-auguste-piccard


<p align="center">
<img src={useBaseUrl('img/piccard1.png')} width="200"/>
</p>

<p align="center">
<img src={useBaseUrl('img/piccard2.png')} width="200"/>
</p>

Posteriormente os balões puderam  alcançar a estratosfera com voos tripulados atingindo 23.000 m em cabines pressurizadas.  Outros voos chegram a 30.000 m utilizando sondas (dispositivos de transmissão de rádio) que transportavam contadores Geiger-Müller para detecção de ionização além de emulsões fotográficas para a determinação do traço, uma espécie de primeira geração de  "laboratório espacial". 



## Câmara de nuvens e a descoberta do pósitron

A câmera de Wilson foi um detector de traços amplamente utilizado em pesquisas de raios
cósmicos e   física nuclear.  Esta câmara, também conhecida como Câmara de nuvens, é constituída por um recipiente com vapor supersaturado (atualmente utiliza-se vapor de álcool), a tal ponto que, quaisquer alterações de pressão ou temperaturas, podem fazer o vapor tornar-se líquido. Na câmara de nuvens, quando o êmbolo é puxado para trás rapidamente, o gás e o vapor na câmera se expandem. A resultante queda de temperatura é suficiente para a condensação do vapor em torno de quaisquer íons presentes no gás. Posteriormente as  câmeras de nuvem foram combinadas com campos magnéticos para desviar as partículas (estudos de carga elétrica). A percepção de Charles Thomson Rees Wilson (1869 - 1959), posteriormente utilizada pelos físicos, foi a de que na ausência de poeiras, as gotículas seriam formadas em torno de partículas carregadas eletricamente, possibilitando o estudo das trajetórias de partículas por meio de fotografias e com isso, concluir sobre sua natureza.  

<p align="center">
<img src={useBaseUrl('img/wilson.png')} width="200"/>
</p>

A descoberta do pósitron, a primeira anti-partícula de matéria a ser identificada em 1932 por Carl Anderson, foi realizada através de uma câmara de nuvens.  Ele havia construído uma câmara de nuvens para determinar a composição dos raios cósmicos. O instrumento incluía um ímã, que permitia a Anderson determinar se as partículas que passavam tinham carga positiva ou negativa, e uma placa de chumbo para desacelerar as partículas. Anderson tirou centenas de fotos de traços provenientes de partículas de raios cósmicos, mas ficou intrigado  com um traço deixado por "algo carregado positivamente e com a mesma massa de um elétron". Depois de quase um ano de esforço e observação, ele decidiu que as traços eram na verdade antielétrons, cada um produzido ao lado de um elétron do impacto de raios cósmicos na câmara de nuvem.  Paul Dirac previu a existência de tal antipartícula em 1931. Anderson recebeu o Prêmio Nobel de Física em 1936 por sua descoberta.


## Emulsões fotográficas e a descoberta do méson $\pi$

A luz, ao incidir sobre uma emulsão fotográfica, produz alterações submicroscópicas que se tornam visíveis após o tratamento químico. O mesmo princípio foi amplamente  utilizado nas décadas de 1930 e 1940 para estudos de raios cósmicos em altitudes de montanhas ou a bordo de  balões.    Quando  partículas rápidas atravessavam uma emulsão fotográfica,  as  alterações submiscrocópicas ocorriam como efeito da interação da radiação com a matéria. 



Entre 1947 e 1948, o físico brasileiro César  Lattes realizou importante trabalho de  estudo dos raios cósmicos estabelecendo  a existência do méson $\pi$.  A descoberta do méson $\pi$ representou um passo fundamental para o entendimento do mecanismo que mantinha  o núcleo atômico   que certamente não poderia ser de natureza elétrica, já que os prótons se repelem uns aos outros. 

Em 1935, Hideki Yukawa propôs uma teoria para explicar as iterações nucleares. Ele sugeriu a existência de uma partícula ainda desconhecida, com uma massa cerca de 200 vezes maior do que a do elétron, que poderia ser emitida e absorvida por prótons e nêutrons. A troca dessa partícula entre os constituintes do núcleo atômico produziria uma atração entre eles, de curto alcance, que poderia explicar a estabilidade nuclear. Por ter uma massa intermediária entre a do elétron e a do próton, recebeu o nome de “méson”. Essas partículas só poderiam existir durante um tempo muito curto, e se desintegrariam fora do núcleo atômico, depois de apenas um bilionésimo de segundo.

Em 1937-38, Carl D. Anderson e Seth H. Neddermeyer encontraram na radiação cósmica, os sinais de algo que parecia ser o méson de Yukawa: tinha uma massa adequada, e se desintegrava do modo previsto. Durante quase dez anos, parecia que tudo se encaixava e que se dispunha de uma boa teoria sobre a constituição da matéria. Em 1947, no entanto, essa tranqüilidade foi derrubada. Descobriu-se que o méson de Anderson e Neddermeyer não tinha o comportamento previsto. 
   
Para poderem explicar as interações nucleares, os mésons deveriam ser fortemente absorvidos por prótons e nêutrons. Previa-se, portanto, que eles fossem facilmente capturados pela matéria. No entanto, um grupo de pesquisadores italianos (Marcello Conversi, Ettore Pancini e Oreste Piccioni) observou que os mésons que haviam sido encontrados na radiação cósmica podiam atravessar centenas de núcleos atômicos sem sofrer nenhuma alteração. Eles tinham uma interação muito fraca com prótons e nêutrons, ao contrário do que se esperava. 



Em 1946, uma equipe de pesquisadores de Bristol (Inglaterra), sob a direção de Cecil F. Powell, estava estudando os traços produzidos por reações nucleares em emulsões fotográficas. Pela análise dos rastros lá deixados por prótons e outras partículas carregadas, era possível determinar a sua energia e massa. Beppo Occhialini e César Lattes analisaram algumas emulsões de um novo tipo, que haviam sido colocadas no alto de uma montanha nos pirineus franceses (o Pic du Midi). Ao revelar e analisar as emulsões, observaram grande número de traços deixados por partículas que interpretaram inicialmente como sendo os mésons já conhecidos. No entanto, após alguns dias de estudo, foram encontrados dois traços especiais, de mésons que iam diminuindo de velocidade e parando; do final desses traços brotava um rastro de um novo méson.  Para obter maior número de dados, Lattes viajou para a Bolívia, e colocou no alto do Monte Chacaltaya, a uma altitude de 5.500 metros, várias emulsões nucleares. Nelas, foi possível encontrar cerca de 30 rastros de mésons duplos. Estudando esses traços, foi possível determinar a massa dos mésons e perceber que havia dois tipos de partículas, com massas diferentes. Existia um tipo de méson que era cerca de 30 a 40% mais pesado do que o outro. Ele se desintegrava e produzia o méson mais leve. A partícula secundária  era a que já era conhecida pelos estudos de Anderson e Neddermeyer, e passou a ser chamada de méson $\mu$ (atualmente, é chamado de múon). O méson primário, mais pesado, era algo novo, desconhecido e foi denominado méson $\pi$ (também chamado de píon).  Lattes, Muirhead, Occhialini e Powell publicaram suas descobertas na 'Nature' na edição de 24 de maio de 1947. Estudos posteriores mostraram que o méson pi tinha uma forte interação com o núcleo atômico, possuindo as características exigidas pela teoria de Yukawa. 


A figura a seguir mostra um dos mésons $\pi$ observados por Lattes em 1947. No Modelo Padrão, o píon é composto por dois quarks ($u\bar{d}$), que, via bóson W+, partícula mediadora da força nuclear fraca, decaem nos léptons múon positivo e neutrino do múon.


Créditos da imagem: https://www.ifi.unicamp.br/~fauth/3RadioatividadeeParticulas/2QuarksLeptonseMediadoras/Qarksleptonsemediadoras.html

<p align="center">
<img src={useBaseUrl('img/FI_D8_T3_I25.jpg')} width="200"/>
</p>



<p align="center">
<img src={useBaseUrl('img/mesonpi2.png')} width="200"/>
</p>

No final de 1947, Lattes deixou Bristol com a intenção de tentar detectar píons produzidos artificialmente no cíclotron de 184 polegadas que havia começado a funcionar em Berkeley, na Califórnia.  Os resultados do trabalho de Lattes mostraram que de fato estavam sendo produzidos mésons. Dois artigos descrevem o método de detecção e os resultados, o primeiro se referindo a mésons negativos, e o segundo a positivos  (_Production of Mesons by the 184-Inch Berkeley Cyclotron_, Science 107 (1948), 270-1; John Burfening, Eugene Gardner, and C.M.G.Lattes, _Positive Mesons Produced by the 184-Inch Berkeley Cyclotron_, Phys. Rev. 75 (1949)). Utilizando o alcance dos píons e sua curvatura em um campo magnético, foi possível estimar as massas como sendo aproximadamente 300 massas do elétron ($m_{\pi} \approx 300 \times m_{e}$).
   
## Cintiladores, fotomultiplicadoras e SiPMs
