---
id: deteccao
title: Detectando múons
sidebar_label: Detectando múons
---
import useBaseUrl from '@docusaurus/useBaseUrl';

A detecção de uma partícula como o múon pode ser entendida como um teste de suas propriedades fundamentais. Uma partícula não se torna evidente até que interaja com o material do aparato experimental de forma mensurável ou decaia em outras partículas detectáveis.  Em nosso experimento, quando os múons atravessam um material denominado cintilador, partículas de luz (fótons) são produzidas e transformadas em sinal elétrico por meio de um ou mais detectores  SiPM (sigla em inglês para _silicon photomultiplier_). O sinal enviado pelos SiPMs é proporcional à soma de todos so fótons detectados. 


## Montagem experimental

Cada estação _Cosmic_ é composta por:

* fotomultiplicadoras de silício SiPMs (SiPM individual: $6$ mm $\times 6$ mm ) que operam em baixa tensão;
* placas de cintilador plástico de $300$ mm $\times 300$ mm $\times 12$ mm;
* eletrônica de _front-end_: responsável pelo processamento analógico do sinal dos SiPMs ( amplificação, formatação e discriminação) e por prover a baixa tensão para operação dos SiPMs;
* módulos de GPS que permitem enviar as coordenadas geográficas de cada estação e sincronizar o tempo da medida entre estações geograficamente dispersas;
* rede de comunicação Wi-Fi

<p align="center">
<img src={useBaseUrl('img/principioDetector.png')} width="200"/>
</p>

## Dados e software para análise 

Os dados são armazenados em sistemas compartilhados na internet ("sistemas em nuvem") e analisados via Jupyter Notebook.  Os dados salvos no banco de dados incluem:

* quais sensores detectaram um sinal naquele evento;
* posição da estação;
* largura do sinal;
* número de eventos detectados;
* informações extras do GPS...



