---
id: anatomia
title: Chuveiros atmosféricos extensos
sidebar_label: Chuveiros atmosféricos extensos
---
import useBaseUrl from '@docusaurus/useBaseUrl';

A natureza dos raios cósmicos  permaneceu um mistério por muito tempo. No momento, sabe-se que a radiação que vemos nas profundezas da atmosfera não são os raios cósmicos primários que entram do espaço. Essas partículas de alta energia que chegam do espaço sideral são principalmente (89%) prótons - núcleos de hidrogênio, o elemento mais leve e comum no universo - mas também incluem núcleos de hélio (10%) e núcleos mais pesados (1%), até o urânio. Ao chegarem à Terra, colidem com os núcleos dos átomos da alta atmosfera, criando mais partículas, principalmente píons. Os píons carregados podem decair rapidamente, emitindo  múons. Ao contrário dos píons, eles não interagem fortemente com a matéria e podem viajar pela atmosfera para penetrar no solo. A taxa de múons que chegam à superfície da Terra é tal que aproximadamente a cada um segundo um múon atravessa  um volume equivalente ao tamanho da cabeça de uma pessoa.

Os píons neutros decaem quase que imediatamente em quanta gama, $\gamma$, de alta energia (fótons ). Fótons de alta energia colidindo com núcleos atômicos podem criar um par de elétrons e positrons. Elétrons e pósitrons interagindo com núcleos carregados produzem os chamados fótons _brehmstrahlung_, que por sua vez pode criar pares, e assim sucessivamente. Às vezes, quando a energia da partícula de raio cósmico primário é muito alta, elétrons, pósitrons e fótons em forma de cascata podem chegar ao solo.

Os píons eletricamente carregados podem interagir ainda mais fortemente, criando várias gerações de partículas secundárias, mas às vezes, especialmente quando a densidade dos núcleos com as quais interage é  pequena (muito alto na atmosfera), eles decaem. Um dos produtos do decaimento do píon é o múon. O múon é um lepton da segunda geração no Modelo Padrão, e como tal se comporta exatamente como um elétron. A única diferença é sua massa. O múon é mais massivo do que o elétron (∼200 vezes).

O "elétron pesado" - o múon - interage eletromagneticamente, e porque ele
é mais massivo, mais difícil é perturbar seu movimento. Quando ele se  move muito rápido,
é muito mais difícil forçá-lo a emitir fótons _brehmstrahlung_. Um múon de alta energia pode
facilmente viajar não só por toda a atmosfera, desde o ponto de origem até a superfície da Terra, mas também penetrar paredes e pisos de concreto. 

Assim sendo um chuveiro atmosférico extenso, se formado a partir de um próton primário de $10^15$ eV irá produzir em torno de $10^6$ partículas,  com esta composição aproximada: 80% delas fótons, 18% elétrons/pósitrons, 1.7% múons e 0.3% de hádrons. 

<p align="center">
<img src={useBaseUrl('img/air_shower.png')} width="200"/>
</p>