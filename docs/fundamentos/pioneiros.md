---
id: pioneiros
title: Os pioneiros
sidebar_label: Os pioneiros
---
import useBaseUrl from '@docusaurus/useBaseUrl';

Em 1911 e 1912, o físico austríaco Victor Hess fez uma série de subidas em um balão de hidrogênio para fazer medições da radiação na atmosfera. Ele estava procurando a fonte de radiação ionizante registrada em um eletrômetro - pois na época assumia-se que  a radiação era proveniente da radioatividade natural presente nas rochas da Terra. Desta forma, acreditava-se que com o aumento da altitude a intensidade da medida da radiação deveria diminuir.  Para testar a teoria, em 1909 o cientista alemão Theodor Wulf mediu a taxa de ionização perto do topo da Torre Eiffel (a uma altura de cerca de 300 metros) usando um eletrômetro portátil. Embora ele esperasse que a taxa de ionização diminuísse com a altura, Wulf notou que a taxa de ionização no topo era pouco menos da metade que no nível do solo - uma diminuição muito menos significativa do que o previsto. 

Victor Hess foi ainda além,  levando eletrômetros em um balão. Em 1911, seu balão atingiu uma altitude de cerca de 1100 metros, mas Hess não encontrou "nenhuma mudança essencial" na quantidade de radiação em comparação com o nível do solo. Então, em 7 de agosto de 1912, no último dos sete voos daquele ano, Hess fez uma subida a 5300 metros. Lá ele descobriu que a taxa de ionização era cerca de três vezes maior que a do nível do mar e concluiu que a radiação penetrante estava entrando na atmosfera vinda de cima. Em um voo anterior, ele não encontrou nenhuma queda perceptível durante um eclipse solar parcial, então ele pode descartar o Sol como a fonte. Hess havia de fato descoberto uma fonte natural de partículas de alta energia: os raios cósmicos. Pela descoberta da radiação cósmica, Viktor Hess recebeu o Prêmio Nobel em 1936.



Créditos das imagens:

_100 Years of Cosmic Rays - A selected chronology from the first ionisation measurements in air to the understanding of cosmic accelerators_. - Deutsches Elektronen-Synchrotron DESY


<p align="center">
<img src={useBaseUrl('img/ballonHess.png')} width="200"/>
</p>


<p align="center">
<img src={useBaseUrl('img/eletrometro2.png')} width="200"/>
</p>











