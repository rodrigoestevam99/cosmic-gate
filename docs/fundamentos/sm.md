---
id: sm
title: O Modelo Padrão da Física de Partículas
sidebar_label: O Modelo Padrão da física de partículas
---
import useBaseUrl from '@docusaurus/useBaseUrl';

As teorias e descobertas de milhares de físicos desde a década de 1930 resultaram em uma visão notável da estrutura fundamental da matéria: tudo no universo é feito de alguns blocos básicos chamados partículas fundamentais, governados por quatro forças fundamentais. Nosso melhor entendimento de como essas partículas e três das forças estão relacionadas entre si está codificado no Modelo Padrão da física de partículas. Esta teoria altamente elegante classifica as partículas elementares de acordo com suas respectivas cargas e descreve como elas interagem por meio de interações fundamentais. Desenvolvido no início dos anos 1970, ele explicou com sucesso quase todos os resultados experimentais e previu com precisão uma ampla variedade de fenômenos. Ao longo de anos  e por meio de muitos experimentos, o Modelo Padrão tem se  estabelecido como uma teoria da física bem corroborada. 

## Partículas de matéria

Toda a matéria ao nosso redor é feita de partículas elementares, os blocos de construção da matéria. Essas partículas ocorrem em dois tipos básicos chamados quarks e léptons. Cada grupo é composto por seis partículas, que se relacionam aos pares, ou “gerações”. As partículas mais leves e estáveis ​​constituem a primeira geração, enquanto as partículas mais pesadas e menos estáveis ​​pertencem à segunda e terceira gerações. Toda matéria estável no universo é feita de partículas que pertencem à primeira geração; quaisquer partículas mais pesadas decaem rapidamente para outras mais estáveis. Os seis quarks são pareados em três gerações - o _quark up_ e o _quark down_ formam a primeira geração, seguido pelo _quark charm_ e _quark strange_, depois o _quark top_ e _quark bottom_ (ou _beauty quark_). Os quarks também vêm em três "cores" diferentes e só se misturam para formar objetos incolores. Os seis léptons são arranjados de forma semelhante em três gerações - o _elétron_ e o _neutrino do elétron_, o _múon_ e o _neutrino do múon_, e o _tau_ e o _neutrino do tau_. O elétron, o múon e o tau têm carga elétrica e massa significativa, enquanto os neutrinos são eletricamente neutros e têm muito pouca massa.

<p align="center">
<img src={useBaseUrl('img/STDMhiggsAndField.png')} width="500"/>
</p>


Fonte: _Particles of the Standard Model of particle physics_ (Image: Daniel Dominguez/CERN)

## Forças e partículas portadoras

Existem quatro forças fundamentais em ação no universo: a _força forte_, a _força fraca_, a _força eletromagnética_ e a _força gravitacional_. Eles funcionam em diferentes faixas e têm diferentes qualidades. A gravidade é a mais fraca, mas tem um alcance infinito. A força eletromagnética também tem alcance infinito, mas é muitas vezes mais forte que a gravidade. As forças fraca e forte são eficazes apenas em um alcance muito curto e dominam apenas no nível das partículas subatômicas. Apesar do nome, a força fraca é muito mais forte do que a gravidade, mas é de fato a mais fraca das outras três. A força forte, como o nome sugere, é a mais forte de todas as quatro interações fundamentais. Três das forças fundamentais resultam da troca de partículas portadoras de força, que pertencem a um grupo mais amplo chamado _bósons_. Partículas de matéria transferem quantidades discretas de energia trocando bósons entre si. Cada força fundamental tem seu próprio bóson correspondente - a força forte é transportada pelo _glúon_, a força eletromagnética é transportada pelo _fóton_ e os _bósons W_ e _Z_ são responsáveis ​​pela força fraca. Embora ainda não tenha sido encontrado, o _gráviton_ deve ser a partícula de gravidade portadora de força correspondente. O modelo padrão inclui as forças eletromagnéticas, fortes e fracas e todas as suas partículas portadoras, e explica bem como essas forças agem sobre todas as partículas de matéria. No entanto, a força mais familiar em nossa vida cotidiana, a gravidade, não faz parte do Modelo Padrão, já que encaixar a gravidade confortavelmente nesta estrutura tem se mostrado um desafio difícil. A teoria quântica usada para descrever o mundo subatômico _micro_ e a teoria geral da relatividade usada para descrever o mundo _macro_ são difíceis de serem encaixadas em uma única estrutura. Torná-las  matematicamente compatíveis no contexto do Modelo Padrão representa ainda um desafio. Mas, felizmente para a física de partículas, quando se trata da escala minúscula das partículas, o efeito da gravidade é tão fraco que chega a ser desprezível. Somente quando a matéria se apresenta em quantidades volumosas, na escala do corpo humano ou dos planetas por exemplo, o efeito da gravidade é significativo. Portanto, o Modelo Padrão ainda funciona bem, apesar de sua exclusão relutante de uma das forças fundamentais.



Texto adaptado de _The Standard Model_ - CERN Science:   [https://home.cern/science/physics/standard-model]
